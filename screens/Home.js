import React, { useState, useEffect } from 'react';
import {StatusBar, View, Text, TextInput, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';

const currencyAPI_URL = "https://api.frankfurter.app"

const fetchCurrencyLatestAPI = () =>{
  return fetch(`${currencyAPI_URL}/latest`)
  .then(res => res.json())
  
  .then(data => Object.keys(data.rates))
}

// const convertCurrencyAPI = (inputAmount, inputCurrency, outputCurrency) =>{
//   return fetch(`${currencyAPI_URL}/latest?amount=${inputAmount}&from=${inputCurrency}&to=${outputCurrency}`)
//   .then(res => res.json())
//   // .then(data => Object.keys(data.rates))
// }

const Home=()=> {
  const [loading, setLoading] = useState(false);
  const [currencyList, setCurrencyList] = useState([]);
  const [open, setOpen] = useState(false);
  const [outputOpen, setOutputOpen] = useState(false);
  const [inputCurrency, setInputCurrency] = useState("");
  const [outputCurrency, setOutputCurrency] = useState("");
  const [inputAmount, setInputAmount] = useState(0);
  const [outputAmount, setOutputAmount] = useState(0);

    useEffect(() => {
      fetchCurrencyLatestAPI()
      fetchCurrency()
      // .then(list => setCurrencyList(list))
    }, [])

    const fetchCurrency = () =>{
      fetchCurrencyLatestAPI()
     .then(list => setCurrencyList(list))
     //.then(data => Object.keys(data.rates))
   }

 const handleConversion= async() =>{
  try {
    setLoading(true);
    const response = await axios.get(
           `${currencyAPI_URL}/latest`
            );
            const exchangeRate = response.data.rates[outputCurrency];
                 const convertedAmount = inputAmount * exchangeRate;
                 setOutputAmount(convertedAmount);
                 setLoading(false);
             } 
    catch (error) {
      console.error(error);
               }
             }

//   const host = 'api.frankfurter.app';
// fetch(`https://${host}/latest?amount=10&from=GBP&to=USD`)
//   .then(resp => resp.json())
//   .then((data) => {
//     alert(`10 GBP = ${data.rates.USD} USD`);
//   });

  return (
    <View style={styles.container}>
      <DropDownPicker 
      nestedScrollEnabled={true} 
          style={{
            // backgroundColor: "crimson"
             borderStyle:"none"
           }}
           containerStyle={{
     //marginVertical :"20%",
     //height:'45px',
     width:'80%',
     justifyContent:'center',
     alignItems:'center',
     fontSize:'20px',
     backgroundColor:"red ",
     marginBottom:"10px",
    //paddingLeft:"15px",
     borderStyle:"none"
           }}
       autoScroll={true}
       placeholderStyle={{
      color: "grey",
      fontWeight: "bold"
    }}
       dropDownContainerStyle={{
  backgroundColor: "#f2f2f2",
  borderStyle:"none",

}}

listMode="SCROLLVIEW"
scrollViewProps={{
  nestedScrollEnabled: true,
}}
      open={open}
        value={inputCurrency}
        items={currencyList.map(currency => ({
          label:currency,
          value:currency,
        }))}
        placeholder="Select Currency"
        setOpen={setOpen}
        setValue={setInputCurrency}
      setItems={setCurrencyList}
        onChangeText={(value) => setInputCurrency(value)}
      />
     
      <TextInput style={styles.inputBox}
        value={inputAmount}
        onChangeText={(text) => setInputAmount(text)}
        keyboardType="numeric"
      />
       <DropDownPicker  
        style={{
          // backgroundColor: "crimson"
           borderStyle:"none"
         }}
         containerStyle={{
   //marginVertical :"20%",
   //height:'45px',
   width:'80%',
   justifyContent:'center',
   alignItems:'center',
   fontSize:'20px',
   backgroundColor:"red ",
   marginBottom:"10px",
   //paddingLeft:"15px",
   borderStyle:"none"
         }}
       disableBorderRadius={true}
       autoScroll={true}
       placeholderStyle={{
      color: "grey",
      fontWeight: "bold"
    }}
       dropDownContainerStyle={{
  backgroundColor: "#dfdfdf",
  

}}
      open={outputOpen}
        value={outputCurrency}
        items={currencyList.map(currency => ({
          label:currency,
          value:currency,
        }))}
        placeholder="Select Currency..."
        setOpen={setOutputOpen}
        setValue={setOutputCurrency}
      setItems={setCurrencyList}
        onChangeText={(value) => setOutputCurrency(value)}
      />
      <Text style={styles.mainText}>{outputAmount}</Text>
      <View style={styles.btnContainer}>
        {loading
        ? <ActivityIndicator color="#000000" size="large"/>
        :
        <TouchableOpacity
         style={styles.btn}
         onPress={handleConversion}
         >
         <Text style={styles.btnText} onPress={handleConversion}>Convert</Text>
         </TouchableOpacity>
}
        </View>
    </View>
  );
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex:1,
   alignItems:'center',
   backgroundColor:"#d5d9e0",
   paddingTop:"40%"

  },
     btn: {
       backgroundColor:'#566f8f',
       height:'50px',
       width:'100%',
       justifyContent:'center',
       alignItems:'center',
       borderRadius:"10px"

     },

     btnText: {
      color:'white',
      fontSize:'20px',
    },
    inputBox: {
      height:'45px',
      width:'80%',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:"10px",
      fontSize:'20px',
      backgroundColor:"white",
      marginBottom:"10px",
      paddingLeft:"15px"
      
    }, 
    mainText: {
      color:'#0c223d',
      fontSize:'30px',
      marginVertical:"20px"
    },
    btnContainer: {
      height:'50px',
       width:'80%',
       justifyContent:'center',
       alignItems:'center',
     // borderColor:'black',
      //borderWidth:2
    },
   
})