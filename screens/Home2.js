import React, { useState } from 'react';
import { View, Text, TextInput, Button } from 'react-native';
import axios from 'axios';

function Home2() {
  const [inputCurrency, setInputCurrency] = useState('USD');
  const [outputCurrency, setOutputCurrency] = useState('EUR');
  const [inputAmount, setInputAmount] = useState(0);
  const [outputAmount, setOutputAmount] = useState(0);

  async function handleConversion() {
    try {
      const response = await axios.get(
        `https://api.frankfurter.app/latest/${inputCurrency}`
      );
      const exchangeRate = response.data.rates[outputCurrency];
      const convertedAmount = inputAmount * exchangeRate;
      setOutputAmount(convertedAmount);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <View>
      <TextInput
        value={inputCurrency}
        onChangeText={(text) => setInputCurrency(text)}
      />
      <TextInput
        value={outputCurrency}
        onChangeText={(text) => setOutputCurrency(text)}
      />
      <TextInput
        value={inputAmount}
        onChangeText={(text) => setInputAmount(text)}
        keyboardType="numeric"
      />
      <Text>{outputAmount}</Text>
      <Button title="Convert" onPress={handleConversion} />
    </View>
  );
}
export default Home2